FROM ruby:2.2.2
RUN  apt-get update -qq && apt-get install -y build-essential postgresql libpq-dev

RUN apt-get install locales
RUN echo 'ru_RU.UTF-8 UTF-8' >> /etc/locale.gen
RUN locale-gen ru_RU.UTF-8
RUN dpkg-reconfigure -fnoninteractive locales
ENV LC_ALL=ru_RU.utf8
ENV LANGUAGE=ru_RU.utf8
RUN update-locale LC_ALL="ru_RU.utf8" LANG="ru_RU.utf8" LANGUAGE="ru_RU"

RUN mkdir -p /apps/scam-test

WORKDIR /apps/scam-test
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install --jobs 3

ADD . /apps/scam-test

EXPOSE 8080
#CMD rake db:create && rake db:migrate && bundle exec rails s -p 8080 -b '0.0.0.0'
CMD bundle exec rails s -p 8080 -b '0.0.0.0'
