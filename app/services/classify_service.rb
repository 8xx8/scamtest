class ClassifyService
  class << self
    def train(text, category, language)
      Classifier.train(text, category)
    end

    def classify(text)
      Classifier.classify(text).to_sym
    end

    def stem(text)
      text = text.mb_chars.downcase

      words = text
        .scan(/(\w+).*?/im)
        .map { |e| e.first }

      text.scan(/\S+@\S+/im).each do |m|
        words << 'has_email'
      end

      text.scan(/(http(s)?\S+)/im).each do |m|
        words << 'has_link'
      end

      stemmer = UEAStemmer.new
      words.map do |w|
        stemmer.stem(w)
      end
    end
  end

end
