require 'test_helper'

class ClassifyServiceTest < ActionController::TestCase
  setup do
  end

  test "stem" do
    features = ClassifyService.stem('http://facebook.com :Hello, I have seen your profile and got interested. I am a single white woman with no kids. I am looking for my second half. If you are interested, e-mail me at yuliyakisonka@gmail.com In case if the site deletes e-mails, another way:yuliyakisonka {at} gmail {dot} comory|u|l|i|y|a|k|i|s|o|n|k|a|@|g|m|a|i|l|.|c|o|m|(delete the slashes)https://ya.ru')

    assert { features.include?('has_link') }
    assert { features.include?('has_email') }
  end
end
