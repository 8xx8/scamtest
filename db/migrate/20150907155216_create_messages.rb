class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :body
      t.string :category
      t.string :language
      t.integer :external_id

      t.timestamps null: false
    end
  end
end
