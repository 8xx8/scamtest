class Message < ActiveRecord::Base
  validates :body, presence: true
  validates :language, presence: true

  def good?
    category.to_sym == :good
  end

  def bad?
    category.to_sym == :bad
  end

  def save
    self.category = ClassifyService.classify(self.body).to_sym
    super
  end
end
