class ClassifierFeature < ActiveRecord::Base
  validates :category, presence: true
  validates :name, presence: true, uniqueness: {:scope => :category}

  def good?
    category.to_sym == :good
  end

  def bad?
    category.to_sym == :bad
  end

  def self.categories
    [:good, :bad]
  end
end
