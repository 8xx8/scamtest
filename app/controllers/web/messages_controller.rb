class Web::MessagesController < Web::ApplicationController
  before_action :set_message, only: [:show, :edit, :update, :destroy]

  # GET /messages
  def index
    query = params[:q] || {}
    @search = Message.ransack query
    @messages = @search.result.page(params[:page]).decorate
  end

  # GET /messages/1
  def show
  end

  # GET /messages/new
  def new
    @message = Message.new
  end

  # GET /messages/1/edit
  def edit
  end

  # POST /messages
  def create
    @message = Message.new(message_params)

    if @message.save
      f(:success)
      redirect_to messages_path
    else
      f(:error)
      render :new
    end
  end

  # PATCH/PUT /messages/1
  def update
    if @message.update(message_params)
      f(:success)
      redirect_to messages_path
    else
      f(:error)
      render :edit
    end
  end

  # DELETE /messages/1
  def destroy
    @message.destroy
    redirect_to messages_url, notice: 'Message was successfully destroyed.'
  end

  def good
    ClassifyService.train(@message.text, :good, @message.language)
    @message.category = :good
    @message.save!

    redirect_to action: :index
  end

  def bad
    ClassifyService.train(@message.text, :bad, @message.language)

    @message.category = :bad
    @message.save!

    redirect_to action: :index
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def message_params
      params.require(:message).permit(:body, :category, :language, :external_id)
    end
end
