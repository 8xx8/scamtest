Rails.application.routes.draw do
  scope module: :web do
    root to: 'messages#index'

    resources :messages do
      patch :good, on: :member
      patch :bad, on: :member
    end

    resources :classifier_features, only: [:index, :edit, :new, :create, :update, :destroy]
  end
end
