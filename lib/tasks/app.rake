# encoding: utf-8
namespace :app do
  desc 'Parse scammed.by'
  task parse_scammed_by: [:environment] do
    File.open('scam.txt', 'a') do |file|
      response = Faraday.get("http://www.datingnmore.com/fraud/scam_spam_russian.htm")
      html = response.body
      page = Nokogiri::HTML(html)
      page.css('.ow_custom_html_widget').each do |e|
        file.write e.text
      end
    end
  end

  desc 'Train classifier'
  task train: [:environment] do
    file = File.expand_path('../../../scam.txt', __FILE__)
    c = 0
    File.read(file).each_line do |line|
      Classifier.train(line, :bad)
      print ' ' + ((c / 11_670.0) * 100).to_i.to_s + '% ' if (c % 100 == 0) && c != 0
      print '.'
      c += 1
    end

    #file = File.expand_path('../../../db/tweets/good', __FILE__)
    #c = 0
    #File.read(file).each_line do |line|
      #Classifier.train(line, :bad)
      #print ' ' + ((c / 10_585.0) * 100).to_i.to_s + '% ' if (c % 100 == 0) && c != 0
      #print '.'
      #c += 1
    #end
  end

  desc 'Train streets'
  task streets: [:environment] do
    buffer = ''

    City::Street.find_each do |city|
      buffer += " #{city.name}"
      if buffer.length > 3000
        10.times do
          print '.'
          Classifier.train(buffer, :good)
        end
        buffer = ''
      end
    end
  end

end
