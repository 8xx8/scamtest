class MessageSerializer < ActiveModel::Serializer
  attributes :id, :body, :category, :language, :external_id
end
