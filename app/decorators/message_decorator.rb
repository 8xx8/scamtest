class MessageDecorator < Draper::Decorator
  delegate_all

  def category_class
    if object.good?
      'label-success'
    else
      'label-danger'
    end
  end
end
